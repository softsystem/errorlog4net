﻿using log4net;
using System;
using System.Reflection;
namespace ErrorLog5.LogUtility
{
    public interface ILogger
    {
        void Debug(string message);
        void Info(string message);
        void Error(string message, Exception ex = null);
    }
    public class Logger : ILogger
    {

        private readonly ILog _logger;
        public Logger()
        {
            //.Config.XmlConfigurator.Configure();
            this._logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        }
        public void Debug(string message)
        {
            this._logger?.Debug(message);
        }
        public void Info(string message)
        {
            this._logger?.Info(message);
        }
        public void Error(string message, Exception ex = null)
        {
            this._logger?.Error(message, ex?.InnerException);
        }
    }
}