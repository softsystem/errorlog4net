﻿using ErrorLog5.LogUtility;
using log4net;
using log4net.Config;
using System;
using System.Data.SqlClient;
using System.IO;
using System.Reflection;
namespace ConsoleLog
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            log4net.Util.LogLog.InternalDebugging = true;

            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("App.config"));
            var demo = new Logger();
            demo.Info("Starting the console application");
            try
            {
                demo.Debug($"Starting {MethodBase.GetCurrentMethod()?.DeclaringType}");
                throw new Exception("Sample Error inside the try catch block code");
            }
            catch (Exception ex)
            {
                demo.Error(ex.Message, ex.InnerException);
            }
            demo.Debug("Waiting for user input");
            //Console.ReadLine();
            demo.Info("Ending application");
            //string connectionString = "data source=.\\sqlexpress;initial catalog=LogsDB;integrated security=true;";
            //using (SqlConnection con = new SqlConnection(connectionString))
            //{
            //    con.Open();
            //    SqlCommand cmd = con.CreateCommand();
            //    cmd.CommandText = "select * from Log4NetLog";
            //    var tt = cmd.ExecuteNonQuery();
            //}
        }
        public void CallLogger()
        {
            
        }

    }
}
