using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace LoggerAPI
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddLog4Net();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                loggerFactory.AddLog4Net();
                app.UseExceptionHandler(errorApp =>
                {
                    errorApp.Run(async context =>
                    {
                       
                        //context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                        //context.Response.ContentType = "text/html";

                        //await context.Response.WriteAsync("<html lang=\"en\"><body>\r\n");
                        //await context.Response.WriteAsync("ERROR!<br><br>\r\n");

                        //var exceptionHandlerPathFeature =
                        //    context.Features.Get<IExceptionHandlerPathFeature>();

                        //if (exceptionHandlerPathFeature?.Error is FileNotFoundException)
                        //{
                        //    await context.Response.WriteAsync("File error thrown!<br><br>\r\n");
                        //}

                        //await context.Response.WriteAsync("<a href=\"/\">Home</a><br>\r\n");
                        //await context.Response.WriteAsync("</body></html>\r\n");
                        //await context.Response.WriteAsync(new string(' ', 512)); // IE padding
                    });
                });
            }
            
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
